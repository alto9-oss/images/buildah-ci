FROM curlimages/curl:7.78.0 AS downloader

ARG TARGETOS="linux"
ARG TARGETARCH="amd64"
ARG KUBECTL_VERSION="1.22.3"
ARG KUSTOMIZE_VERSION="4.4.1"

WORKDIR /downloads

RUN set -ex; \
    curl -fL https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/${TARGETOS}/${TARGETARCH}/kubectl -o kubectl && \
    chmod +x kubectl

RUN set -ex; \
    curl -fL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_${TARGETOS}_${TARGETARCH}.tar.gz | tar xz && \
    chmod +x kustomize

FROM python:3.9-slim-bullseye

ARG TARGETOS
ARG TARGETARCH
ARG KUBECTL_VERSION
ARG KUSTOMIZE_VERSION

COPY --from=downloader /downloads/kubectl /usr/local/bin/kubectl
COPY --from=downloader /downloads/kustomize /usr/local/bin/kustomize

RUN apt-get update && apt-get install -y podman buildah amazon-ecr-credential-helper curl
# Install the AWS CLI
RUN pip3 --no-cache-dir install awscli==1.17.10
# Setup default registry list
RUN echo "[registries.search]" >> /etc/containers/registries.conf
RUN echo "registries = ['quay.io', 'docker.io']" >> /etc/containers/registries.conf